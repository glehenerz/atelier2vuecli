import config from './config.js';

export default class DB {
  static apiURL = config.apiURL;

  static async init() {
    try {
      const localTodos = this.getAll();
      const localTimestamp = localStorage.getItem('todosTimestamp');
      
      const response = await fetch(this.apiURL + "/todos");
      const apiTodos = await response.json();

      const apiLatestTimestamp = apiTodos.reduce((latest, todo) => {
        const todoTimestamp = new Date(todo.createdAt);
        return todoTimestamp > latest ? todoTimestamp : latest;
      }, new Date(0));

      if (!localTimestamp || apiLatestTimestamp > new Date(localTimestamp)) {
        // Les données de l'API sont plus récentes
        this.setTodos(apiTodos, apiLatestTimestamp);
      } else {
        // Les données locales sont plus récentes ou égales
        // (Optionnel) Mettre à jour l'API avec les données locales si nécessaire
      }
    } catch (error) {
      console.error("Erreur lors de l'initialisation des données :", error);
    }
  }

  static setTodos(todos, timestamp) {
    localStorage.setItem('todos', JSON.stringify(todos));
    localStorage.setItem('todosTimestamp', timestamp.toISOString());
  }

  static getAll() {
    const storedTodos = localStorage.getItem('todos');
    return storedTodos ? JSON.parse(storedTodos) : [];
  }

  static async findAll() {
    try {
      const response = await fetch(this.apiURL + "/todos");
      if (!response.ok) {
        throw new Error(`Erreur HTTP ! statut : ${response.status}`);
      }
      const todos = await response.json();
      return todos;
    } catch (error) {
      console.error("Erreur lors de la récupération des todos:", error);
      return this.getAll(); // Retourner les données du localStorage en cas d'erreur
    }
  }

  static async addOne(data) {
    try {
      const response = await fetch(this.apiURL + "/todos", {
        method: 'post',
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      });
      if (!response.ok) {
        throw new Error(`Erreur HTTP ! statut : ${response.status}`);
      }
      const newTodo = await response.json();
      this.updateLocalStorage(newTodo); // Mise à jour du localStorage
      return newTodo;
    } catch (error) {
      console.error("Erreur lors de l'ajout d'un todo:", error);
    }
  }

  static async updateOneById(id, data) {
    try {
      const response = await fetch(`${this.apiURL}/todos/${id}`, {
        method: 'put',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      });

      if (!response.ok) {
        throw new Error(`Erreur HTTP ! statut : ${response.status}`);
      }
      const updatedTodo = await response.json();
      this.updateLocalStorage(updatedTodo); // Mise à jour du localStorage
      return updatedTodo;
    } catch (error) {
      console.error("Une erreur est survenue lors de la mise à jour :", error);
    }
  }

  static async deleteOneById(id) {
    try {
      const response = await fetch(`${this.apiURL}/todos/${id}`, {
        method: 'DELETE',
      });
      if (!response.ok) {
        throw new Error(`Erreur HTTP ! statut : ${response.status}`);
      }
      await response.json();
      this.removeFromLocalStorage(id); // Mise à jour du localStorage
    } catch (error) {
      console.error("Une erreur est survenue lors de la suppression :", error);
    }
  }

  static updateLocalStorage(updatedTodo) {
    const todos = this.getAll();
    const index = todos.findIndex(t => t.id === updatedTodo.id);
    if (index !== -1) {
      todos[index] = updatedTodo;
    } else {
      todos.push(updatedTodo);
    }
    this.setTodos(todos, new Date(updatedTodo.createdAt));
  }

  static removeFromLocalStorage(id) {
    const todos = this.getAll();
    const updatedTodos = todos.filter(t => t.id !== id);
    localStorage.setItem('todos', JSON.stringify(updatedTodos));
  }
}
